These are my public dotfiles which can be used with `stow` to bootstrap
a new installation, e.g.:

    git clone --recursive https://github.com/kkremitzki/dotfiles ~/.dotfiles
    cd $TARGET
    stow -R .
    cd .config/nvim/bundle/dein.vim 
    git checkout master #FIXME: Submodule comes on a commit, not a branch; why?
    vim -c ":call dein#update() | :q" # Install all vim plugins
