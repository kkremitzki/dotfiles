" Fix issue with transparent terminal vim having non-transparent background
function! AdaptColorscheme()
    highlight clear CursorLine
    highlight Normal ctermbg=none
    highlight LineNr ctermbg=none
    highlight Folded ctermbg=none
    highlight NonText ctermbg=none
    highlight SpecialKey ctermbg=none
    highlight VertSplit ctermbg=none
    highlight SignColumn ctermbg=none
endfunction

au ColorScheme * call AdaptColorscheme()

" Color
set background=dark
let g:gruvbox_italic=1
colorscheme gruvbox

