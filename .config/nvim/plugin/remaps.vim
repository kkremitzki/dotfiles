let mapleader = "\<Space>"

"Edit & source vimrc; navigate to sourced files"
nnoremap <leader>ev :tabnew ~/.config/nvim<cr>:tcd ~/.config/nvim<cr>
nnoremap <leader>sv :source ~/.vimrc<cr>:echo "Sourcing vimrc..."<cr>

"Files & Navigation"
map <C-n> :NERDTreeToggle<CR>

" Open help in new tab
cnoreabbrev <expr> h getcmdtype() == ":" && getcmdline() == "h" ? "tab h" : "h"

"Tagbar"
nnoremap <leader>tt :TagbarToggle<cr>

"fzf.vim"
nnoremap <leader>bu :<C-u>Buffers<cr>
nnoremap <leader>o :<C-u>Files<cr>
nnoremap <Leader>/ :<C-u>Rg<cr>

"Fugitive.vim"
nnoremap <leader>gb :Gblame -w<cr>
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gd :Gvdiff<cr>

"Misc Remaps"
nnoremap <Leader>w :w<cr>
nnoremap <Leader>q :q<cr>
nnoremap <Leader>x :x<cr>

" Neomake
nnoremap <leader>mb :Neomake! branch<cr>
nnoremap <leader>mt :Neomake! branch_test<cr>
nnoremap <leader>me :Neomake! branch_run<cr>
nnoremap <leader>ml :NeomakeListJobs<cr>

" Debugging
nnoremap <silent> <c-b> :GdbToggleBreak<cr>

" Python
" Should be a better way to handle Python breakpoints
" Would be nice to be unified with C++ breakpoint mgmt
nnoremap <leader>pb oimport pdb; pdb.set_trace()<Esc>
nnoremap <leader>pr <Plug>(iron-send-motion)
