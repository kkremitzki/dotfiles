" NCM2
" enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()

" IMPORTANT: :help Ncm2PopupOpen for more information
set completeopt=noinsert,menuone,noselect

" path to directory where libclang.so can be found
let g:ncm2_pyclang#library_path = '/usr/lib/llvm-7/lib'

" a list of relative paths for compile_commands.json
let g:ncm2_pyclang#database_path = [
            \ 'compile_commands.json',
	    \ 'build/compile_commands.json'
	    \ ]

" a list of relative paths looking for .clang_complete
let g:ncm2_pyclang#args_file_path = ['.clang_complete']

" go to definition
autocmd FileType c,cpp nnoremap <buffer> gd :<c-u>call ncm2_pyclang#goto_declaration()<cr>

" increase refresh rate for vim-gitgutter, tagbar, etc.
set updatetime=100
